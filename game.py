from random import randint

name = input("Hi! What is your name?")



for guess_num in range(5):

    guess_month = randint(1,12)
    guess_year = randint(1924, 2004)

    print("Guess", guess_num + 1, ":", name, "were you born in", guess_month, "/", guess_year)

    user_guess = input("Did I guessed it right? yes or no?").lower()

    if user_guess == "yes":
        print("I knew it")
        break
    elif user_guess == "no" and guess_num < 5:
        print("Drat! Lemme try again!")
    else:
        print("I have other things to do. Good bye.")
